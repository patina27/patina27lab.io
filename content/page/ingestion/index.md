---
title: Ingestion
subtitle: Movies, animes, books, games, etc.
comments: true
---

*大致上按照时间顺序排列，点击查看短评。 

## Ongoing...

{{< gallery caption-effect="none" >}}
  {{< col_img src="wataten" caption="天使降临到我身边"
      alt="阿伟の暴毙系列。。">}}
  {{< col_img src="kimetsu" caption="鬼灭之刃">}}
  {{< col_img src="Botchan" caption="哥儿" alt="">}}
  {{< col_img src="fushengliuji" caption="浮生六记" alt="">}}
  {{< col_img src="zen_and_motorcycle" caption="Zen and the Art of Motorcycle Maintenance" 
      alt="哲学课堂。。读了好久都没读完">}}
  {{< col_img src="pokemon_sm" caption="Pokemon Sun & Moon: Anime Series" 
      alt="放飞自我的剧本、优秀的人设、不错的演出...种种理由使我重新爱上了古老的宝可梦系列。阿罗拉赛高！">}}
  {{< col_img src="HollowKnight" caption="Hollow Knight"
      alt="满分的银河恶魔城。手残地狱。">}}
  {{< col_img src="osu" caption="osu!"
      alt="最近突然想试试看音游。。卡在了普通难度上不去" >}}
{{< /gallery >}}

## Seen that!

{{< gallery caption-effect="none" >}}
  {{< col_img src="pancreas" caption="我想吃掉你的胰脏" 
      alt="Serendipity! 日式个人主义的现代演绎。">}}
  {{< col_img src="my_hero_academia" caption="我的英雄学院" 
      alt="看完灵能百分百以后对动画作画产生了巨大的兴趣，就去看了雄英。BONES社的一流作画看得很过瘾，只是总体来看原作人设和剧本比较弱了，尤其是反派的处理上。不过总的来说如果定位是相对子供的话就无妨了。">}}
  {{< col_img src="mob" caption="Mob Psycho 100" 
      alt="灵能百分百是一拳再上一个台阶。ONE老师对剧情和人物的拿捏更进一层。每个角色都有立体感，并随着剧情的推进成长与改变，这是在高度脸谱化的业内难以见到的。BONES社顶级的Animation更是如虎添翼。真心希望BD能卖好。">}}
  {{< col_img src="the_heart" caption="心" 
      alt="《心》的短评详见博文。">}}
  {{< col_img src="I_am_cat" caption="我是猫" 
      alt="《我是猫》的短评也请详见博文 :)">}}
  {{< col_img src="spiderverse" caption="Spider-Man: Into the Spider-Verse" 
      alt="漫画风格的动画别树一帜。美国的作画技术再次证明了他世界领先的地位。我认为传统的平面作画之于3D的CG作画的优势是艺术表达形式的多样和形体的夸张，而事实是在世界的某些地方这样的差距已经越来越小了。日本业界还需要继续探索CG作画的可能性。希望有一天这么一套流畅的2.5D感的CG动画也能用在日式的故事表达上。">}}
  {{< col_img src="liz_and_bluebird" caption="利兹与青鸟" 
      alt="大二秋季学期结束前最后一天看了这部电影，看完以后当场买了BD。我热爱细腻的情感表达，利兹与青鸟做的不但是到位，甚至是有些过火。利兹与青鸟描述的复杂的情感所流露出的美，令我回味。铠塚霙与伞木希美在我看来是瑕疵的、是「disjoint」。当然也有CP党会有反对意见，但我认为原作的朦胧处理表示其对各种解读是包容的。生活中所有的关系也都是充满了遗憾与瑕疵的，这部作品让我感受到了残缺的美感。所谓的「伪物」也可以熠熠生辉。">}}
  {{< col_img src="violet_evergarden" caption="Violet Evergarden"
      alt="这部宇宙圣经最后也是毁誉参半。我是半年以后去补的番，在话题消散之后，这部番的观影体验出奇的好。一个相对好把控的故事背景框架，配上一段段的感动人心的小故事，只应那些跨越时空的话题，不需要太标新立异、也能打动人心。">}}
  {{< col_img src="tamakolovestory" caption="玉子爱情故事 & 玉子市场" 
      alt="补京阿尼的作品的时候发现的宝藏。废萌故事透露了日本阶级固化的社会现状（划掉！）。京都脸看多了真的会上瘾。">}}
  {{< col_img src="soundeuphonium" caption="京吹"
      alt="京吹看完以后后劲很足，请小心服用。青春励志群像剧，这个本来不是很对我胃口的类型，为了买京阿尼的面子（也可能说是 Shawn 的面子）把两季看完了。总的来说，京吹很写实，从人物、情节、各种意义上而言。写实也能做出有趣的动画，这有点打破我的 norm。作为一个曾经的 OIer 京吹里面很多情节我都还是有通感的。期待 2019 的剧场版终章。">}}
  {{< col_img src="hinamatsuri" caption="超能力女儿" 
      alt=" feel 社特别会做生活喜剧。超能力女儿可以说改编的相当成功了。一口气写了这么多短评以后，发现果然描绘人物是最重要的。几个立体的人设没有故事也能撑起一部剧。期待第二部。">}}
  {{< col_img src="ssss_gridman" caption="SSSS.GRIDMAN" 
      alt="不多说了。作为多年的奥迷两泪汪汪。本番诞生了我 Waifu。一如 Trigger 社传统，剧情有烂尾倾向，但是这都无所谓，诞生了平成最后、最强JK才是最重要的。">}}
  {{< col_img src="kotenbu" caption="冰菓"
      alt="Officially 入坑京阿尼的第一部作品，2018年冬季补的番。冰菓这部作品讲小清新侦探体，背后也流露了一个不那么小清新的社会。正经小说改编的格局还是和轻小说不一样。题外话，我也很想保持一颗对事物的好奇心，但我无时无刻不畏惧未知、更畏惧被人发现我的无知。不克服心结是无法成为好奇宝宝的。">}}
  {{< col_img src="weed" caption="野草" 
      alt="野草让我重新发现了鲁迅。真不愧是白话文大师，我觉得现在也没有哪个诗人能达到他的白话文水平。野草我读的似懂非懂，很多意象让人确实摸不着头脑。将情感抽象成某种具体的表达对我来说实在是太难。现实中我连直球都打不好，更玩不出什么花样了。打算什么时候再去读一下梦十夜看看。">}}
  {{< col_img  src="and_the_earth_did_not_devour_him" caption="...And the Earth Did Not Devour Him"   
      alt="Writing Identity 课上要求阅读的书本的其中之一。说是小说还是朦胧的散文诗还是魔幻现实主义呢？英文里似乎叫「vignettes」。 虽然 Identity Study 我学得很痛苦，但是我认可 Identity 是一个相当有分量的概念。" >}}
  {{< col_img src="oregairu" caption="春物" 
      alt="这部番我想写很多东西。最近传出了第三季制作的消息，那么就等第三季完结再写个长评吧！">}}
  {{< col_img src="zelda" caption="Zelda: Breath of the Wild" 
      alt="玩过的最好玩的游戏了。开放世界的典范。Breath of the Wild 这个标题名副其实。游戏中后期完全激发了我的探索之魂。边打字边发出了任豚的猪叫。">}}
  {{< col_img src="newgame" caption="New Game!!"
      alt="芳文社的这部职场系列两季做的也是很用心。996 社畜的日常看来也没这么可怕嘛（划掉）。">}}
{{< /gallery >}}