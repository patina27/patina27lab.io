---
title: 博客更新
date: 2019-04-29
tags: ["blog-update"]
---

Main takeaways: \
① 加入了一个 Ingestion 模块来记录影视短评，欢迎推荐作品。\
② 更新了主题，更换了显示字体。

<!--more-->

最近看到 Google fonts 终于出了中文字体，就借机把这个博客的字体问题解决的。比较好奇他们是怎么解决中文字体的体积问题， ~~还没有找到什么答案~~ （Update：Google也是用了 Unicode-range，把 44,683 个字符根据统计数据分成了 102 份，render 的时候只会读取页面需要的区块。详见[链接](https://fonts.google.com/earlyaccess#Noto+Sans+SC+Sliced)。）这个实现在别的中文在线字体库里也是类似的。我对衬线字体有特殊的偏好，所以正文都还是喜欢用宋体风格的，看不惯的请多多包涵。

一直想在博客里加入 Fractal128 曾经存在的豆瓣影视记录之类的东西。很可惜 Hugo 并没有现成的轮子。去隔壁的 Hexo 那边看一下 [hexo-douban](<https://github.com/mythsman/hexo-douban>) 这个轮子，发现他直接 parse 了豆瓣网站... 比较粗暴了。豆瓣的API似乎只支持比较简单的查询操作（豆瓣的开发者文档已经在一年前关闭了），如果要用爬取网站 html 的话简单的用 Ajax call 可以预见的就会非常缓慢，而本地提取然后本地生成似乎可行，但是对于我在 GitLab 用 CI/CD 就增加了难度。综上考虑我放弃造轮子的想法，而是做了一个简单的独立展示页面，取名[「Ingestion」](/page/ingestion)，放在了菜单栏里。还耗费了相当长的时间给每个列表上的作品写了些小短评，用脑过度了（笑）。欢迎留言推荐作品！