---
title: Why I switched to Hugo
subtitle: ...or why Hexo is painful to use
date: 2018-10-27
tags: ["hexo", "hugo", "development"]
---
Well, I've been playing around with Static Site Generators recently. As you can see from the footnote, I changed the framework of my blog into Hugo, an SSG written by Go-lang. My first experience with the Hugo framework was at my summer research internship. I was asked to build a simple website for a research project. So I planned to choose an SSG framework as the content generator and develop themes on top of it. As a previous Hexo user, I would normally dive into Hexo... and soon I quit. There are several reasons behind it.

<!--more-->

- First, the documentation of Hexo is too simplified, it was just hard to reckon from the text how the contents are structured. When compared with Hugo's ridiculously extensive documentation, Hexo really discourages me from developing upon it.

- Hexo is written in Node.js. It is a great language, but it sure has limitations. After all, it is a scripting language, so to use Hexo I need to install Node.js, npm, and type a couple of commands to finally able to generate my site. It might not be a big trouble if the site is only a personal blog. But if the site involves multiple authors, the thing gets troublesome. Plus, just admit it that Node.js is not so easy to install, as I personally have gone into a lot of trouble while installing it. I believe those Ph.D. students will find it really painful to maintain the website if they got to deal with another scripting language.

- Hexo is hard to migrate or collaborate. SSG used to be less than ideal when it comes to writing articles on multiple machines. Thanks to CI/CD pipelines, now we can essentially generate the website on the service provider whenever we push the raw files, making collaboration and migration simpler. But Hexo is not as good even with CI/CD pipelines. Whenever I clone the content base and try to generate the site locally, Hexo always requires me to initiate a new site. It looks like Hexo cannot detect (or does not have a detection mechanism) that the current folder contains exactly the structured files it needs to generate sites. So the only solution is to create a new folder and initiate a Hexo project there, then copy the configuration, all the articles, themes, etc. to the new folder. It is really problematic since now you need to overwrite your content base.

- Hexo is too specific to blogs. The content management structure is poorly documented and not super extensible to other site structures such as a product demo page.

Well, hard to believe that I have so many complaints on Hexo. Anyway, let's move on to Hugo. Hugo basically had all the merits that Hexo doesn't have. It has a great documentation specific to an extent that it tells you how Hugo prioritize ambiguous logic. It is written by Go, and thanks god it is a compiled language which means no hustle to install any interpreters and dependencies. And it is easy to migrate the site. The best thing about Hugo is super flexible content structure support. It allows various setups of the website, making it adaptive to scenarios beyond blogs. Overall Hugo looks more legit as a framework upon which I can better develop my own structure.
