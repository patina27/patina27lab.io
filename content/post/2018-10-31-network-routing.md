---
title: Notes on Network Routing Protocols 
date: 2018-10-31
tags: ["coursework", "network", "routing"]
draft: true
---

Somehow I messed up on this course, Intro to Computer Networks. By far, I got a pretty average grade on midterm and I missed a lab. Moreover, I find it hard to concentrate in class. To pull myself back from the impeding fail, I got to make some faithful effort to catch up. So here comes the study notes.

<!--more-->

# Internet Routers and the IPv4 Protocol

## Forwarding & IPv4 Addressing

- When a packet arrives at a router, the router must move the packet to an output link. This is called forwarding. Basically, every router has a *forwarding table*. 

- The *Internet* is a "network of networks", in which each network own a range of IP addresses. Routers only know about address ranges. Packet forwarding is based on identifying the _best matching route_ for a packet's IP address, which is the *longest prefix* matching route.


- Subnet: a.b.c.d/k: k specifies the number of relevant prefix bits in a.b.c.d, which sometimes known as subnet mask. For example:

```
158.130.52.9/14 = 10011110.100000**.********.******** = 158.128.0.0/14
```

- 